from django.apps import AppConfig


class HealthInsuranceClaimsConfig(AppConfig):
    name = 'health_insurance_claims'
