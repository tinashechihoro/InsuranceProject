from django.db import models


# Create your models here.
class Customer(models.Model):
    GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    MERITAL_STATUS = (
        ('S', 'Single'),
        ('M', 'Married'),
        ('D', 'Divorced'),
        ('W', 'Widow'),
    )

    first_name = models.CharField(max_length=50, help_text='first name')
    last_name = models.CharField(max_length=50, help_text='last name')
    date_of_birth = models.DateTimeField(help_text='date of birth')
    gender = models.CharField(max_length=1, choices=GENDER)
    marital_status = models.CharField(max_length=1, choices=MERITAL_STATUS)
    address = models.CharField(max_length=200, blank=True)
    mobile_number = models.CharField(max_length=50, help_text='cell phone number')
    email_address = models.EmailField(blank=True)
    national_identity_number = models.CharField(max_length=100)
    passport_number = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.first_name


class Application(models.Model):
    APPLICATION_STATUS = (
        ('A', 'Approved'),
        ('D', 'Denied'),
        ('W', 'Wiating'),
    )
    customer_id = models.ForeignKey(Customer)
    vehicle_id = models.CharField(max_length=50)
    application_status = models.CharField(max_length=1, choices=APPLICATION_STATUS)
    coverage = models.CharField(max_length=100)

    def __str__(self):
        return self.application_status


class Qoute(models.Model):
    application_id = models.ForeignKey(Application)
    customer_id = models.ForeignKey(Customer)
    issue_date = models.DateTimeField(auto_now=True)
    valid_from_date = models.DateTimeField(auto_now_add=True)
    valid_till_date = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=200)
    product_id = models.CharField(max_length=100)
    coverage_level = models.CharField(max_length=50)

    def __str__(self):
        return self.description


class InsurancePolicy(models.Model):
    application_id = models.ForeignKey(Application)
    customer_id = models.ForeignKey(Customer)
    department_name = models.CharField(max_length=100)
    policy_number = models.CharField(max_length=100)
    start_date = models.DateTimeField(auto_now=True)
    expiry_date = models.DateTimeField(auto_now_add=True)
    terms_conditions_description = models.TextField()
    def __str__(self):
        return self.terms_conditions_description


class Policy(models.Model):
    pass
class Agreement(models.Model):
    pass


class PremiumPayment(models.Model):
    customer_id = models.ForeignKey(Customer)
    policy_number = models.ForeignKey(Policy)
    premium_payment_schedule = models.DateTimeField(auto_now_add=True)
    premium_payment_amount = models.DecimalField(max_digits=12, decimal_places=6)
    receipt_number = models.CharField(max_length=20)

    def __str__(self):
        return self.premium_payment_amount


class Vehicle(models.Model):
    customer_id = models.ForeignKey(Customer)
    policy_id = models.ForeignKey(Policy)
    dependent_national_identity_number = models.CharField(max_length=100)
    vehicle_registration_number = models.CharField(max_length=100)
    vehicle_value = models.DecimalField(max_digits=12, decimal_places=6)
    vehicle_type = models.CharField(max_length=100)
    vehicle_size = models.CharField(max_length=100)
    vehicle_number_of_seats = models.IPAddressField()
    vehicle_manufacturer = models.CharField(max_length=100)
    vehicle_engine_number = models.CharField(max_length=100)
    vehicle_chasis_number = models.CharField(max_length=100)
    vehicle_number = models.CharField(max_length=100)
    vehicle_model_number = models.CharField(max_length=100)

    def __str__(self):
        return  self.vehicle_registration_number


class Claim(models.Model):
    CLAIM_STATUS = (
        ('A', 'Approved'),
        ('D', 'Denied'),
        ('W', 'Wiating'),
    )
    customer_id = models.ForeignKey(Customer)
    agreement_id = models.CharField(max_length=100)
    claim_amount = models.DecimalField(max_length=12, decimal_places=6)
    incident_id = models.CharField(max_length=100)
    damage_type = models.CharField(max_length=100)
    date_of_claim = models.DateTimeField(auto_now_add=True)
    claim_status = models.CharField(max_length=1, choices=CLAIM_STATUS)

    def __str__(self):
        return  self.claim_amount


class ClaimSettlement(models.Model):
    claim_id = models.ForeignKey(Claim)
    customer_id = models.ForeignKey(Customer)
    vehicle_id = models.CharField(max_length=50)
    date_settled = models.DateTimeField(auto_now=True)
    amount_paid = models.DecimalField(max_digits=12, decimal_places=6)
    coverage_id = models.CharField(max_length=100)

    def __str__(self):
        return  self.amount_paid


class Staff(models.Model):
    GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    MERITAL_STATUS = (
        ('S', 'Single'),
        ('M', 'Married'),
        ('D', 'Divorced'),
        ('W', 'Widow'),
    )

    company_name = models.ForeignKey(Company)
    first_name = models.CharField(max_length=50, help_text='first name')
    last_name = models.CharField(max_length=50, help_text='last name')
    date_of_birth = models.DateTimeField(help_text='date of birth')
    gender = models.CharField(max_length=1, choices=GENDER)
    marital_status = models.CharField(max_length=1, choices=MERITAL_STATUS)
    address = models.CharField(max_length=200, blank=True)
    mobile_number = models.CharField(max_length=50, help_text='cell phone number')
    email_address = models.EmailField(blank=True)
    national_identity_number = models.CharField(max_length=100)
    passport_number = models.CharField(max_length=100, blank=True)
    staff_qualifications = models.TextField()
    staff_salary = models.DecimalField(max_length=12, decimal_places=6)
    def __str__(self):
        return  self.first_name


class Department(models.Model):
    name = models.CharField(max_length=100, blank=True)
    company = models.ForeignKey(Company)
    office = models.CharField(max_length=100, blank=True)
    contact_information = models.CharField(max_length=200, blank=True)
    # passport_number = models.CharField(max_length=100, blank=True)
    leader = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return  self.name


class Office(models.Model):
    name = models.CharField(max_length=100, blank=True)
    department_name = models.ForeignKey(Department)
    company_name = models.ForeignKey(Company)
    contact_information = models.CharField(max_length=200, blank=True)
    address = models.CharField(max_length=100, blank=True)
    staff = models.ManyToManyField(Staff)

    def __str__(self):
        return  self.name


class Membership(models.Model):
    customer_id = models.ForeignKey(Customer)
    membership_type = models.CharField(max_length=100)
    organisation = models.CharField(max_length=100)
    def __str__(self):
        return  self.membership_type


class VehicleService(models.Model):
    vehicle_id = models.ForeignKey(Vehicle)
    customer_id = models.ForeignKey(Customer)
    department_name = models.CharField(max_length=100)
    vehicle_service_address = models.CharField(max_length=100)
    vehicle_service_contact = models.CharField(max_length=100)
    vehicle_service_incharge = models.CharField(max_length=100)
    vehicle_service_type = models.CharField(max_length=100)

    def __str__(self):
        return  self.vehicle_service_type


class NextOfKin(models.Model):
    GENDER = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    MERITAL_STATUS = (
        ('S', 'Single'),
        ('M', 'Married'),
        ('D', 'Divorced'),
        ('W', 'Widow'),
    )

    agreement_id = models.ForeignKey(Agreement)
    application_id = models.ForeignKey(Application)
    customer_id = models.ForeignKey(Customer)

    # company_name = models.ForeignKey(Company)
    first_name = models.CharField(max_length=50, help_text='first name')
    last_name = models.CharField(max_length=50, help_text='last name')
    date_of_birth = models.DateTimeField(help_text='date of birth')
    gender = models.CharField(max_length=1, choices=GENDER)
    marital_status = models.CharField(max_length=1, choices=MERITAL_STATUS)
    address = models.CharField(max_length=200, blank=True)
    mobile_number = models.CharField(max_length=50, help_text='cell phone number')
    email_address = models.EmailField(blank=True)
    national_identity_number = models.CharField(max_length=100)

    def __str__(self):
        return  self.first_name


class Receipt(models.Model):
    premium_payment_id = models.ForeignKey(PremiumPayment)
    customer_id = models.ForeignKey(Customer)
    cost = models.DecimalField(decimal_places=6,max_length=12)
    datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return  self.cost


class Incident(models.Model):
    incident_type = models.CharField(max_length=100)
    incident_date = models.DateTimeField(auto_now=True)
    description = models.TextField()
    def __str__(self):
        return  self.incident_type



class IncidentReport(models.Model):
    incident_id = models.ForeignKey(Incident)
    customer_id = models.ForeignKey(Customer)
    incident_inspector = models.CharField(max_length=200)
    incident_cost = models.DecimalField(decimal_places=6, max_digits=12)
    incident_type = models.CharField(max_length=100)
    incident_description = models.TextField()

    def __str__(self):
        return  self.incident_type



class Coverage(models.Model):
    company_name = models.ForeignKey(Company)
    coverage_amount = models.DecimalField(decimal_places=6, max_length=12)
    coverage_type = models.CharField(max_length=100)
    coverage_level = models.CharField(max_length=100)
    product_id = models.CharField(max_length=100)
    coverage_description = models.TextField()
    coverage_terms = models.TextField()

    def __str__(self):
        return  self.coverage_type


class PolicyRenewable(models.Model):
    agreement_id = models.ForeignKey(Agreement)
    application_id = models.F(Application)
    customer_id = models.ForeignKey(Customer)
    date_of_renewal = models.DecimalField(auto_created=True)
    type_of_renewal = models.CharField(max_length=100)

    def __str__(self):
        return  self.type_of_renewal


class Product(models.Model):
    company_name = models.ForeignKey(Company)
    name = models.CharField(max_length=100)
    product_price = models.DecimalField(decimal_places=6,max_length=12)
    product_type = models.CharField(max_length=100)

    def __str__(self):
        return  self.name


class Company(models.Model):
    name = models.CharField(max_length=100, help_text='çompany name')
    address = models.CharField(max_length=100, help_text='address')
    contact_number = models.CharField(max_length=100, help_text='cell number')
    email_dress = models.EmailField()
    website = models.URLField()
    location = models.CharField(max_length=100, help_text='location')
    department_name = models.CharField(max_length=100, help_text='department')
    office_name = models.CharField(max_length=100, help_text='office name')

    def __str__(self):
        return  self.name

class Insurance_Policy_Coverage(models.Model):
    coverage_id = models.ForeignKey(Coverage)

    def __str__(self):
        return  self.coverage_id
