from django.apps import AppConfig


class PolicyAdministrationConfig(AppConfig):
    name = 'policy_administration'
