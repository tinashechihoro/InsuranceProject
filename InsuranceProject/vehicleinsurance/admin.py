from django.contrib import admin
from vehicleinsurance.models import Application, Company, PremiumPayment, Incident, Coverage, Customer, Vehicle, Staff, \
    Department, Claim, Agreement, ClaimSettlement, IncidentReport, Insurance_Policy_Coverage, InsurancePolicy, \
    Membership, NextOfKin, Office, Policy, PolicyRenewable, Product, Qoute, Receipt, VehicleService


class ApplicationModelAdmin(admin.ModelAdmin):
    list_display = [ 'customer_id', 'vehicle_id']
    list_filter = [ 'customer_id', 'vehicle_id']
    search_fields = [ 'customer_id', 'vehicle_id']
admin.site.register(Application,ApplicationModelAdmin)

class CompanyModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'website', 'contact_number']
    list_filter = ['name',  'website', 'contact_number']
    search_fields = ['name',  'website', 'contact_number']
admin.site.register(Company,CompanyModelAdmin)



class PremiumPaymentModelAdmin(admin.ModelAdmin):
    list_display = ['premium_payment_amount', 'receipt_number']
    list_filter = ['premium_payment_amount', 'receipt_number']
    search_fields = ['premium_payment_amount', 'receipt_number']
admin.site.register(PremiumPayment,PremiumPaymentModelAdmin)

class IncidentModelAdmin(admin.ModelAdmin):
    list_display = ['incident_type', 'description']
    list_filter = ['incident_type', 'description']
    search_fields = ['incident_type', 'description']
admin.site.register(Incident, IncidentModelAdmin)


# class CoverageModelAdmin(admin.ModelAdmin):
#     list_filter = [ 'coverage_type', 'coverage_description']
#     list_filter = [ 'coverage_type', 'coverage_description']
#     search_fields = [ 'coverage_type', 'coverage_description']
# admin.site.register(Coverage,CoverageModelAdmin)


class CustomerModelAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'mobile_number', 'mobile_number']
    list_filter = ['date_of_birth', 'last_name', 'gender']
    search_fields = ['first_name', 'last_name', 'mobile_number', 'mobile_number']

admin.site.register(Customer,CustomerModelAdmin)


class VehicleModelAdmin(admin.ModelAdmin):
    list_display = ['vehicle_registration_number', 'vehicle_value', 'vehicle_manufacturer', 'vehicle_engine_number',
                    'vehicle_model_number', 'dependent_national_identity_number']
    list_filter = ['vehicle_registration_number', 'vehicle_value']
    search_fields = ['vehicle_registration_number', 'vehicle_value', 'vehicle_manufacturer', 'vehicle_engine_number',
                     'vehicle_model_number', 'dependent_national_identity_number']


class StaffModelAdmin(admin.ModelAdmin):
    list_display = ['incident_type', 'description']
    list_filter = ['incident_type', 'description']
    search_fields = ['incident_type', 'description']


# class DepartmentMOdelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
class ClaimModelAdmin(admin.ModelAdmin):
    list_display = ['damage_type']
    list_filter = ['claim_amount']
    search_fields = ['damage_type']

admin.site.register(Claim, ClaimModelAdmin)
#
#
# class AgreementMOdelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class ClaimSettlementAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class IncidentReportModelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class InsurancePolicyCoverageModelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class InsurancePolicyModelAdmin(admin.ModelAdmin):
#
#
#  class MembershipModelAdmin(admin.ModelAdmin):
#         list_display = ['incident_type', 'description']
#         list_filter = ['incident_type', 'description']
#         search_fields = ['incident_type', 'description']
#
#
# class NextOfKinModelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class OfficeModelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class PolicyModelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class PolicyRenewableModelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class ProductMOdelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
class QouteModelAdmin(admin.ModelAdmin):
    list_display = ['customer_id', 'description']
    list_filter = ['customer_id', 'description']
    search_fields = ['customer_id', 'description']
admin.site.register(Qoute, QouteModelAdmin)
#
#
# class ReceiptModelAdmin(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
#
#
# class VehicleService(admin.ModelAdmin):
#     list_display = ['incident_type', 'description']
#     list_filter = ['incident_type', 'description']
#     search_fields = ['incident_type', 'description']
