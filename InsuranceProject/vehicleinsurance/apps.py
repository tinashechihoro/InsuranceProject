from django.apps import AppConfig


class VehicleinsuranceConfig(AppConfig):
    name = 'vehicleinsurance'
