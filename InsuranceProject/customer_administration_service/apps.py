from django.apps import AppConfig


class CustomerAdministrationServiceConfig(AppConfig):
    name = 'customer_administration_service'
