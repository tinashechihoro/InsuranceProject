from django.apps import AppConfig


class ClaimAdministrationServiceConfig(AppConfig):
    name = 'claim_administration_service'
