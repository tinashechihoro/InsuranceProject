from django.contrib import admin

from .models import PersonalDetails,Beneficiaries,PackagesDetailsAndBenefits,Notes


class PersonalDetailsModelAdmin(admin.ModelAdmin):
    list_display = ['first_name','surname','mobile_number']
    list_filter = ['first_name','surname','mobile_number']
    search_fields = ['first_name','surname','mobile_number']
    #date_hierarchy = ['date_of_birth',]
    ordering = ['first_name','surname']

class BeneficiariesModelAdmin(admin.ModelAdmin):
    list_display = ['first_name','surname']
    list_filter = ['first_name','surname']
    search_fields = ['first_name','surname']
    #date_hierarchy = ['date_of_birth',]
    ordering = ['first_name','surname']


admin.site.register(PersonalDetails,PersonalDetailsModelAdmin)
admin.site.register(Beneficiaries,BeneficiariesModelAdmin)

admin.site.register(Notes)