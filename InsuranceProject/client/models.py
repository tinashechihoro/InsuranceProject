from django.db import models


# Create Client Application models

class PersonalDetail(models.Model):
    surname = models.CharField(max_length=100,help_text="last name")
    first_name = models.CharField(max_length=100, help_text="first name")
    maiden = models.CharField(max_length=100, help_text="first name")
    title = models.CharField(max_length=100, help_text="first name")
    date_of_birth = models.DateField()
    nrc_id = models.CharField(max_length=50)
    work_address =  models.CharField(max_length=100, help_text="work address")
    email_address = models.EmailField(help_text="your email")
    mobile_number = models.CharField(max_length=100, help_text="cell number")
    period_of_insurance_start = models.DateField()
    period_of_insurance_end = models.DateField()

    class Meta:




    def __str__(self):
       return self.first_name


class InsuranceType(models.Model):
      name = models.CharField(max_length=100)


class PackagesDetailAndBenefits(models.Model):
    insurance_type = models.CharField(InsuranceType,max_length=100)
    name  = models.CharField(max_length=100,help_text="package name")
    type = models.CharField(max_length=100, help_text="package type")
    premium = models.DecimalField(decimal_places=6,max_digits=9)
    active = models.BooleanField(default=True)

class Beneficiaries(models.Model):

    surname = models.CharField(max_length=100,help_text="last name")
    first_name = models.CharField(max_length=100, help_text="first name")
    maiden = models.CharField(max_length=100, help_text="first name")
    title = models.CharField(max_length=100, help_text="first name")
    date_of_birth = models.DateField()
    nrc_id = models.CharField(max_length=50)
    member_id = models.CharField(PersonalDetails,max_length=100)

class Notes(models.Model):
    notes = models.TextField()
