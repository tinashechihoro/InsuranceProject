from django.conf.urls import url,include
from .views import render

urlpatterns = [
    # ex: /polls/
    url(r'^$', home, name='home'),
    # ex: /polls/5/
    # url(r'^(?P<Customer_id>[0-9]+)/$', views.detail, name='detail'),
    # # ex: /polls/5/results/
    # url(r'^(?P<Customer_id>[0-9]+)/results/$', views.results, name='results'),
    # # ex: /polls/5/vote/
    # url(r'^(?P<Customer>[0-9]+)/vote/$', views.vote, name='vote'),
]