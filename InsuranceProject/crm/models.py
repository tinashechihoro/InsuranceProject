from django.db import models


# Customer model
class Customer(models.Model):
    company_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    marrige_status = models.CharField()
    home_address = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    business_address = models.CharField(max_length=100)
    office_phone_number = models.CharField(max_length=100)
    cell_number = models.CharField(max_length=50)
    business_email_address = models.CharField(max_length=100)
    personal_email_address = models.CharField(max_length=100)


class Address(models.Model):
    pass


# class Country(models.Model):
#     pass


class Product(models.Model):
    product_name = models.CharField(max_length=100)
    supplier_id = models.ForeignKey(Supplier)
    # category_id = models.ForeignKey(Cu)
    quantity_per_unit = models.IntegerField()
    unit_price = models.IntegerField()
    units_in_stock = models.IntegerField()
    unit_on_order = models.IntegerField()
    re_oredr_level = models.IntegerField()
    discontinued = models.IntegerField()


class Shipper(models.Model):
    company_name = models.CharField(max_length=100)
    phone_number = models.Model()
    email_address = models.EmailField()
    # website = models.URLField()#


class Employee(models.Model):
    last_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    birth_day = models.DateField()
    hire_date = models.DateField()
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    Region = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=100)
    county = models.CharField(max_length=100)
    # image = models.Pi
    home_phone = models.CharField(max_length=100)
    email_address = models.CharField(max_length=100)
    # notes = models.TtField()
    reports_to = models.CharField(max_length=100)


class OrderDetail(models.Model):
    product_id = models.ForeignKey(Products)
    quantity_per_unit = models.IntegerField()
    unit_price = models.IntegerField()
    disccount = models.IntegerField()


class Order(models.Model):
    customer_id = models.ForeignKey(Customer)
    employee_id = models.ForeignKey(Employees)
    order_date = models.DateField()
    required_date = models.DateField()
    shipping_date = models.DateField()
    ship_via = models.IntegerField()
    freight = models.IntegerField()
    ship_name = models.CharField(max_length=100)
    ship_address = models.CharField(max_length=100)
    ship_city = models.CharField(max_length=100)
    ship_region = models.CharField(max_length=100)
    ship_postal_code = models.CharField(max_length=100)
    ship_country = models.CharField(max_length=100)


class Category(models.Model):
    category_name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)


class Supplier(models.Model):
    company_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    business_address = models.CharField(max_length=100)
    office_phone_number = models.CharField(max_length=100)
    cell_number = models.CharField(max_length=50)
    business_email_address = models.CharField(max_length=100)
    personal_email_address = models.CharField(max_length=100)
