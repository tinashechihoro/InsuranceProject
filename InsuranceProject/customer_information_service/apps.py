from django.apps import AppConfig


class CustomerInformationServiceConfig(AppConfig):
    name = 'customer_information_service'
