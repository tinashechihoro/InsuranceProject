from django.apps import AppConfig


class ClaimRegistrationServiceConfig(AppConfig):
    name = 'claim_registration_service'
