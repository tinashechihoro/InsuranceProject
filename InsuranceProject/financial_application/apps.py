from django.apps import AppConfig


class FinancialApplicationConfig(AppConfig):
    name = 'financial_application'
