from django.apps import AppConfig


class ClaimsPaymentsServiceConfig(AppConfig):
    name = 'claims_payments_service'
